-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 17, 2021 at 04:45 PM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brief_projects_data_simple`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_dscpt` longtext NOT NULL,
  `dev_ids` int DEFAULT NULL,
  `client_id` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `project_name`, `project_dscpt`, `dev_ids`, `client_id`) VALUES
(1, 'Make tea', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare sem vitae sem dapibus, vitae pharetra lectus accumsan. Aenean faucibus quam turpis, a lobortis felis iaculis in. Donec rhoncus condimentum elit, vitae sodales elit. Curabitur tincidunt nulla ac tempus lacinia. Sed vel malesuada orci. Donec faucibus turpis at mauris scelerisque, et eleifend sapien dapibus. Mauris mollis lacinia mi sed tristique. Duis rhoncus convallis felis vitae efficitur. Cras orci elit, condimentum vitae lacinia vitae, condimentum eu orci. Donec venenatis sed dolor eget viverra. Sed vulputate malesuada mattis. Cras porttitor tellus ipsum, ultrices tempor arcu interdum vitae. Nullam porttitor congue lorem. Integer imperdiet cursus dolor sed eleifend. Aenean vitae leo eget ligula volutpat feugiat. Curabitur molestie hendrerit nisl, ut accumsan massa euismod ut.\r\n\r\nCurabitur rhoncus vestibulum finibus. Praesent elementum tempor lectus nec imperdiet. Phasellus placerat ultrices nisl vitae consequat. Donec maximus ligula sit amet neque eleifend congue. Aenean sollicitudin odio at efficitur porttitor. Sed imperdiet aliquam lectus, eu vulputate dui ultricies sit amet. Quisque in ullamcorper libero, ac tincidunt ligula. Etiam eu purus eu arcu fermentum ornare et a est.\r\n\r\nNulla et lorem ante. Nunc faucibus condimentum ullamcorper. Pellentesque pulvinar a turpis eget ullamcorper. Nullam vitae enim non diam sagittis congue. Ut sit amet massa augue. Suspendisse imperdiet dui ante, porta condimentum sapien volutpat sed. Phasellus eu placerat sem, vel mattis quam. Sed lorem enim, viverra sit amet facilisis sagittis, maximus in nulla. Ut hendrerit, odio semper laoreet congue, lectus lectus lobortis quam, a accumsan sapien nisl non nisi.\r\n\r\nNam nec ante elit. Etiam sed felis molestie, aliquet lectus id, commodo nisl. In sed quam quis elit vehicula scelerisque et et neque. Sed urna quam, lobortis nec sem non, accumsan fringilla lorem. Integer placerat condimentum lorem, et aliquet neque feugiat id. Vivamus venenatis sagittis tortor, accumsan vehicula turpis viverra vitae. Donec porta nisl iaculis malesuada luctus. Aliquam a diam et magna pellentesque tincidunt ut ut sem.\r\n\r\n', NULL, NULL),
(2, 'Clean table', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare sem vitae sem dapibus, vitae pharetra lectus accumsan. Aenean faucibus quam turpis, a lobortis felis iaculis in. Donec rhoncus condimentum elit, vitae sodales elit. Curabitur tincidunt nulla ac tempus lacinia. Sed vel malesuada orci. Donec faucibus turpis at mauris scelerisque, et eleifend sapien dapibus. Mauris mollis lacinia mi sed tristique. Duis rhoncus convallis felis vitae efficitur. Cras orci elit, condimentum vitae lacinia vitae, condimentum eu orci. Donec venenatis sed dolor eget viverra. Sed vulputate malesuada mattis. Cras porttitor tellus ipsum, ultrices tempor arcu interdum vitae. Nullam porttitor congue lorem. Integer imperdiet cursus dolor sed eleifend. Aenean vitae leo eget ligula volutpat feugiat. Curabitur molestie hendrerit nisl, ut accumsan massa euismod ut.\r\n\r\nCurabitur rhoncus vestibulum finibus. Praesent elementum tempor lectus nec imperdiet. Phasellus placerat ultrices nisl vitae consequat. Donec maximus ligula sit amet neque eleifend congue. Aenean sollicitudin odio at efficitur porttitor. Sed imperdiet aliquam lectus, eu vulputate dui ultricies sit amet. Quisque in ullamcorper libero, ac tincidunt ligula. Etiam eu purus eu arcu fermentum ornare et a est.\r\n\r\nNulla et lorem ante. Nunc faucibus condimentum ullamcorper. Pellentesque pulvinar a turpis eget ullamcorper. Nullam vitae enim non diam sagittis congue. Ut sit amet massa augue. Suspendisse imperdiet dui ante, porta condimentum sapien volutpat sed. Phasellus eu placerat sem, vel mattis quam. Sed lorem enim, viverra sit amet facilisis sagittis, maximus in nulla. Ut hendrerit, odio semper laoreet congue, lectus lectus lobortis quam, a accumsan sapien nisl non nisi.\r\n\r\nNam nec ante elit. Etiam sed felis molestie, aliquet lectus id, commodo nisl. In sed quam quis elit vehicula scelerisque et et neque. Sed urna quam, lobortis nec sem non, accumsan fringilla lorem. Integer placerat condimentum lorem, et aliquet neque feugiat id. Vivamus venenatis sagittis tortor, accumsan vehicula turpis viverra vitae. Donec porta nisl iaculis malesuada luctus. Aliquam a diam et magna pellentesque tincidunt ut ut sem.\r\n\r\n', NULL, NULL),
(7, 'dfewgtrrr', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare sem vitae sem dapibus, vitae pharetra lectus accumsan. Aenean faucibus quam turpis, a lobortis felis iaculis in. Donec rhoncus condimentum elit, vitae sodales elit. Curabitur tincidunt nulla ac tempus lacinia. Sed vel malesuada orci. Donec faucibus turpis at mauris scelerisque, et eleifend sapien dapibus. Mauris mollis lacinia mi sed tristique. Duis rhoncus convallis felis vitae efficitur. Cras orci elit, condimentum vitae lacinia vitae, condimentum eu orci. Donec venenatis sed dolor eget viverra. Sed vulputate malesuada mattis. Cras porttitor tellus ipsum, ultrices tempor arcu interdum vitae. Nullam porttitor congue lorem. Integer imperdiet cursus dolor sed eleifend. Aenean vitae leo eget ligula volutpat feugiat. Curabitur molestie hendrerit nisl, ut accumsan massa euismod ut.\r\n\r\nCurabitur rhoncus vestibulum finibus. Praesent elementum tempor lectus nec imperdiet. Phasellus placerat ultrices nisl vitae consequat. Donec maximus ligula sit amet neque eleifend congue. Aenean sollicitudin odio at efficitur porttitor. Sed imperdiet aliquam lectus, eu vulputate dui ultricies sit amet. Quisque in ullamcorper libero, ac tincidunt ligula. Etiam eu purus eu arcu fermentum ornare et a est.\r\n\r\nNulla et lorem ante. Nunc faucibus condimentum ullamcorper. Pellentesque pulvinar a turpis eget ullamcorper. Nullam vitae enim non diam sagittis congue. Ut sit amet massa augue. Suspendisse imperdiet dui ante, porta condimentum sapien volutpat sed. Phasellus eu placerat sem, vel mattis quam. Sed lorem enim, viverra sit amet facilisis sagittis, maximus in nulla. Ut hendrerit, odio semper laoreet congue, lectus lectus lobortis quam, a accumsan sapien nisl non nisi.\r\n\r\nNam nec ante elit. Etiam sed felis molestie, aliquet lectus id, commodo nisl. In sed quam quis elit vehicula scelerisque et et neque. Sed urna quam, lobortis nec sem non, accumsan fringilla lorem. Integer placerat condimentum lorem, et aliquet neque feugiat id. Vivamus venenatis sagittis tortor, accumsan vehicula turpis viverra vitae. Donec porta nisl iaculis malesuada luctus. Aliquam a diam et magna pellentesque tincidunt ut ut sem.\r\n\r\n', NULL, NULL),
(9, 'Drink water', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare sem vitae sem dapibus, vitae pharetra lectus accumsan. Aenean faucibus quam turpis, a lobortis felis iaculis in. Donec rhoncus condimentum elit, vitae sodales elit. Curabitur tincidunt nulla ac tempus lacinia. Sed vel malesuada orci. Donec faucibus turpis at mauris scelerisque, et eleifend sapien dapibus. Mauris mollis lacinia mi sed tristique. Duis rhoncus convallis felis vitae efficitur. Cras orci elit, condimentum vitae lacinia vitae, condimentum eu orci. Donec venenatis sed dolor eget viverra. Sed vulputate malesuada mattis. Cras porttitor tellus ipsum, ultrices tempor arcu interdum vitae. Nullam porttitor congue lorem. Integer imperdiet cursus dolor sed eleifend. Aenean vitae leo eget ligula volutpat feugiat. Curabitur molestie hendrerit nisl, ut accumsan massa euismod ut.\r\n\r\nCurabitur rhoncus vestibulum finibus. Praesent elementum tempor lectus nec imperdiet. Phasellus placerat ultrices nisl vitae consequat. Donec maximus ligula sit amet neque eleifend congue. Aenean sollicitudin odio at efficitur porttitor. Sed imperdiet aliquam lectus, eu vulputate dui ultricies sit amet. Quisque in ullamcorper libero, ac tincidunt ligula. Etiam eu purus eu arcu fermentum ornare et a est.\r\n\r\nNulla et lorem ante. Nunc faucibus condimentum ullamcorper. Pellentesque pulvinar a turpis eget ullamcorper. Nullam vitae enim non diam sagittis congue. Ut sit amet massa augue. Suspendisse imperdiet dui ante, porta condimentum sapien volutpat sed. Phasellus eu placerat sem, vel mattis quam. Sed lorem enim, viverra sit amet facilisis sagittis, maximus in nulla. Ut hendrerit, odio semper laoreet congue, lectus lectus lobortis quam, a accumsan sapien nisl non nisi.\r\n\r\nNam nec ante elit. Etiam sed felis molestie, aliquet lectus id, commodo nisl. In sed quam quis elit vehicula scelerisque et et neque. Sed urna quam, lobortis nec sem non, accumsan fringilla lorem. Integer placerat condimentum lorem, et aliquet neque feugiat id. Vivamus venenatis sagittis tortor, accumsan vehicula turpis viverra vitae. Donec porta nisl iaculis malesuada luctus. Aliquam a diam et magna pellentesque tincidunt ut ut sem.\r\n\r\n', NULL, NULL),
(10, 'Code in php', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare sem vitae sem dapibus, vitae pharetra lectus accumsan. Aenean faucibus quam turpis, a lobortis felis iaculis in. Donec rhoncus condimentum elit, vitae sodales elit. Curabitur tincidunt nulla ac tempus lacinia. Sed vel malesuada orci. Donec faucibus turpis at mauris scelerisque, et eleifend sapien dapibus. Mauris mollis lacinia mi sed tristique. Duis rhoncus convallis felis vitae efficitur. Cras orci elit, condimentum vitae lacinia vitae, condimentum eu orci. Donec venenatis sed dolor eget viverra. Sed vulputate malesuada mattis. Cras porttitor tellus ipsum, ultrices tempor arcu interdum vitae. Nullam porttitor congue lorem. Integer imperdiet cursus dolor sed eleifend. Aenean vitae leo eget ligula volutpat feugiat. Curabitur molestie hendrerit nisl, ut accumsan massa euismod ut.\r\n\r\nCurabitur rhoncus vestibulum finibus. Praesent elementum tempor lectus nec imperdiet. Phasellus placerat ultrices nisl vitae consequat. Donec maximus ligula sit amet neque eleifend congue. Aenean sollicitudin odio at efficitur porttitor. Sed imperdiet aliquam lectus, eu vulputate dui ultricies sit amet. Quisque in ullamcorper libero, ac tincidunt ligula. Etiam eu purus eu arcu fermentum ornare et a est.\r\n\r\nNulla et lorem ante. Nunc faucibus condimentum ullamcorper. Pellentesque pulvinar a turpis eget ullamcorper. Nullam vitae enim non diam sagittis congue. Ut sit amet massa augue. Suspendisse imperdiet dui ante, porta condimentum sapien volutpat sed. Phasellus eu placerat sem, vel mattis quam. Sed lorem enim, viverra sit amet facilisis sagittis, maximus in nulla. Ut hendrerit, odio semper laoreet congue, lectus lectus lobortis quam, a accumsan sapien nisl non nisi.\r\n\r\nNam nec ante elit. Etiam sed felis molestie, aliquet lectus id, commodo nisl. In sed quam quis elit vehicula scelerisque et et neque. Sed urna quam, lobortis nec sem non, accumsan fringilla lorem. Integer placerat condimentum lorem, et aliquet neque feugiat id. Vivamus venenatis sagittis tortor, accumsan vehicula turpis viverra vitae. Donec porta nisl iaculis malesuada luctus. Aliquam a diam et magna pellentesque tincidunt ut ut sem.\r\n\r\n', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `project_name` (`project_name`),
  ADD UNIQUE KEY `single_project` (`id`,`project_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
