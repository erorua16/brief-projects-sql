<?php 
$title_page = $page_title?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <title> <?= $title_page ?></title>
</head>
<body>
    <header> 
    <img src="../images/simplon.png" alt="The logo of simplon. a black circle outline with a black column in the middle">    
    <h1>Project Management</h1>
    </header>