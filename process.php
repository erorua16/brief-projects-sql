<?php 
require "connection.php";


//*****************************************************************QUERIES FOR PROJECT*****************************************************************//










/////////////////////////////////////////////////////////////SELECT ALL FROM PROJECT TABLE/////////////////////////////////////////////////////////////




try{
    $sql = $conn->prepare("SELECT * FROM brief_projects_data_simple.projects");
    $sql->execute();
    $result = $sql->fetchAll(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
    echo "Error : ".$e->getMessage();
}



//////////////////////////////////////////////////////////////////////// SELECT INDIV PROJECT////////////////////////////////////////////////////////////////






if(isset($_REQUEST['idv_project_details_page'])){
    try{
        $sql1 = $conn->prepare("SELECT * FROM brief_projects_data_simple.projects WHERE id = :idv_id");
        $sql1->bindParam('idv_id',$id, PDO::PARAM_INT);
        $sql1->execute();
        $result1 = $sql1->fetchAll(PDO::FETCH_ASSOC);
        forEach($result1 as $res1 ){
            $idv_name = $res1['project_name'];
            $idv_descript = $res1['project_dscpt'];
           }
    }
    catch(PDOException $e){
        echo "Error : ".$e->getMessage();
    }
}






///////////////////////////////////////////////////////////////ADD TO PROJECT TABLE/////////////////////////////////////////////////////////////S






if (isset($_REQUEST['save'])){
    $add_project_name = !empty ($_REQUEST['add_edit_name']) ? $_REQUEST['add_edit_name'] : NULL;
    $add_project_dscpt = !empty ($_REQUEST['add_edit_descript']) ? $_REQUEST['add_edit_descript'] : NULL;
    if (!empty ($add_project_dscpt && $add_project_name)){
        try {
            $sql2 = $conn->prepare("INSERT INTO brief_projects_data_simple.projects (project_name, project_dscpt) VALUES (:new_name, :new_dscpt)");
            $sql2->bindParam('new_name', $add_project_name, PDO::PARAM_STR);
            $sql2->bindParam('new_dscpt', $add_project_dscpt, PDO::PARAM_STR);
            $sql2->execute();
            header("location:../index.php");
            }catch(PDOException $e){
                echo "Error : ".$e->getMessage();
            }
    }
}





///////////////////////////////////////////////////////////////DELETE FROM PROJECT TABLE/////////////////////////////////////////////////////////////S





if(isset($_REQUEST['delete_project'])){
    $delete_project_id = !empty ($_REQUEST['delete_project']) ? $_REQUEST['delete_project'] : NULL ;
    if (!empty ($delete_project_id)){
        try{
            $sql3 = $conn->prepare("DELETE FROM brief_projects_data_simple.projects WHERE id = :delete_id ");
            $sql3->bindParam('delete_id', $delete_project_id, PDO::PARAM_INT);
            $sql3->execute();
            header("location:../index.php");
        }catch(PDOException $e){
            echo "Error : ".$e->getMessage();
        }
    }
};





////////////////////////////////////////////////////////////////////////SELECTED PROJECT FOR MODIFY PROJECT////////////////////////////////////////////////////////////////





if(isset($_REQUEST['edit_project_page'])){
    try{
        session_start();
        $sql4 = $conn->prepare("SELECT * FROM brief_projects_data_simple.projects WHERE id = :idv_id");
        $sql4->bindParam('idv_id',$id_edit, PDO::PARAM_INT);
        $sql4->execute();
        $count4 = $sql4->rowCount();
        $result4 = $sql4->fetchAll(PDO::FETCH_ASSOC);
        forEach($result4 as $res4 ){
            $edit_name = $res4['project_name'];
            $edit_descript = $res4['project_dscpt'];
            $editing_id = $res4['id'];
           }
    }
    catch(PDOException $e){
        echo "Error : ".$e->getMessage();
    }
}





////////////////////////////////////////////////////////////////////////MODIFY PROJECT////////////////////////////////////////////////////////////////





    if(isset($_REQUEST['edit'])){
        $editing_id2 = $_SESSION['id_edit'];
        $update_name = !empty ($_REQUEST['add_edit_name']) ? $_REQUEST['add_edit_name'] : NULL;
        $update_descript = !empty ($_REQUEST['add_edit_descript']) ? $_REQUEST['add_edit_descript'] : NULL;
        if(!empty($update_name && $update_descript)){
           try {  
            $sql5 = $conn->prepare("UPDATE brief_projects_data_simple.projects SET project_name = :updated_project_name , project_dscpt = :updated_descrpt WHERE id= :edit_id");
            $sql5->bindParam('updated_project_name', $update_name, PDO::PARAM_STR);
            $sql5->bindParam('updated_descrpt', $update_descript, PDO::PARAM_STR);
            $sql5->bindParam('edit_id', $editing_id2, PDO::PARAM_INT);
            $sql5->execute();
            $edit_done = 'done';
            session_destroy();
           }
           catch (PDOException $e){
               echo "Error : ".$e->getMessage();
           }
        }
    }






?>