<!--///////////////////////////////////////////////////////////////////HEADER//////////////////////////////////////////////////////////////-->
<?php 
session_start();
$page_title = "Project Management | Simplon.co";
require "../connection.php";
require "../process.php";
require "../include/header.php";


///////MESSAGE IF PROJECT HAS BEEN MODIFIED////////
if (!empty($edit_done)) {
echo "Your project has been modified";
}
session_destroy();




/////////////////////////////////////////////////////////////////////PROJECT REDIRECTION///////////////////////////////////////////////////





if (isset($_REQUEST['add_project_page'])) {
    ///////ADD PROJECT PAGE///////
    require "../routes_project/add_modify_project.php";
}elseif(isset($_REQUEST['edit_project_page'])){
    ///////EDIT PROJECT PAGE///////
    require "../routes_project/add_modify_project.php";
}elseif(isset($_REQUEST['idv_project_details_page'])){
    ///////DETAILS OF PROJECT PAGE///////
    require "../routes_project/idv_project_details.php";
}else{
    ///////MAIN PROJECT PAGE///////
    require "../routes_project/main_project.php";
}




//////////////////////////////////////////////////////////////////FOOTER//////////////////////////////////////////////////////////////
require "../include/footer.php";
?>
