# PROJECT MANAGEMENT BRIEF

## TO START 
1.  Input your informations in the `connection.php` file.
2.  Connect to your local host and import the included database (`projects.sql`). 
3.  Go inside the `public` folder and open your localhost server from there.

## FUNCTIONALITY
1.  `index.php`, the main page, will link to the different files in the folder `routes.project`. The default page is `main_project.php` which displays a table of all the currently existing projects.
2.  The button `edit` will change `index.php` to include `add_modify_project.php` which will allow you to change the `project name` and the `project description`.
3.  The button `add` will change `index.php` to include `add_modify_project.php` which will allow you to `add a new project`.
4.  The button `details` will change `index.php` to include `idv_project_details.php` which will allow you to see the selected project's name and description in more details. 
4.  The button `delete` will `delete` the selected project.

## IMPORTANT NOTE 
1.  In order to avoid unnecessary duplicates, a project cannot be added if it shares the exact same name (case sensitive) as an already existing project.

## FURTHER DEVELOPMENT
1.  In order to instal necessary packages input in terminal the following command: `npm i`.
2.  Compile scss to css with the terminal command: `npm run comp`.
